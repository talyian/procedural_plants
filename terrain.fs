module ProcPlant.Terrain
open DawnLib.Base
open OpenTK
open OpenTK.Graphics

let grid N =
    let makevert x y =
        let pos = new Vector3(float32 x, float32 y, 0.f) * 0.1f
        let col = new Color4(randf 1.0f, randf 1.0f,randf 1.0f, 1.0f)
        new Vertex(pos=pos, col=col)
    let verts = Array2D.init N N (fun x y -> makevert x y) |> Seq.cast<Vertex> |> Seq.toArray
    let indices =
        let pt x y = x * N + y
        [ for x in 0..N-2 do
          for y in 0..N-2 do
          yield! [ pt x y
                   pt (x+1) y
                   pt x (y+1)
                   pt (x+1) y
                   pt x (y+1)
                   pt (x+1) (y+1) ]]
    let m = new Mesh()
    m.indices.AddRange indices
    m.vertices.AddRange verts
    m
