module ProcPlant.Radiance

open System.IO
open System
open OpenTK

type Run = | Run of byte | Dump of byte
with static member parse b = if b > 128uy then Run(b &&& 127uy) else Dump(b)

let load_floatgz (stream:Stream) =
    let gz = new Compression.GZipStream(stream, Compression.CompressionMode.Decompress)
    let bin = new BinaryReader(gz)
    let data = bin.ReadBytes(9999 * 9999)
    let fdata = Array.zeroCreate<float32> (data.Length / 4)
    Buffer.BlockCopy(data, 0, fdata, 0, data.Length)
    printfn "%A" (fdata.Length, data.Length)
    fdata

let load (stream:Stream) =
    let text = new StreamReader(stream)
    assert (text.ReadLine() = "\x23?RADIANCE")
    let headers = Seq.unfold (fun s -> match text.ReadLine() with | "" -> None | s -> Some(s,s)) ""
    for x in headers do printfn "%A" x

    let w, h = let p = text.ReadLine().Split() in int p.[1], int p.[3]

    let bin = new BinaryReader(stream)
    let rle_data = bin.ReadBytes(9999 * 9999)
    let bin = new BinaryReader(new MemoryStream(rle_data))
    printfn "rle_data %A" (rle_data.Length, rle_data.[0..30])

    let read_rle_chunk () =
        match Run.parse (bin.ReadByte()) with
            | Run(len) -> let b = bin.ReadByte() in Array.create (int len) b
            | Dump(len) -> bin.ReadBytes(int len)

    let rec read_bytes len oldData =
        if len < 0 then failwith "read too many bytes"
        elif len = 0 then Array.concat (List.rev oldData)
        else let data = read_rle_chunk ()
             read_bytes (len - data.Length) (data::oldData)

    let read_row () =
        let combine r g b e =
            let mutable r = float32 r
            let mutable g = float32 g
            let mutable b = float32 b
            let mutable e = int e
            while max r (max g b) > 1.0f do
                r <- r / 2.0f
                g <- g / 2.0f
                b <- b / 2.0f
                e <- e + 1
            e <- e - 127
            (r * 2.0f ** float32 e,
             g * 2.0f ** float32 e,
             b * 2.0f ** float32 e)
        let xx, yy, c =
            try
                bin.ReadByte(), bin.ReadByte(), int(bin.ReadByte())*256+int (bin.ReadByte())
            with
                | :? IO.EndOfStreamException as e -> 2uy, 2uy, 0
        if xx <> 2uy || yy <> 2uy then failwith "unrecognized row error"
        let r = read_bytes c []
        let g = read_bytes c []
        let b = read_bytes c []
        let e = read_bytes c []
        Array.init c (fun i -> combine (r.[i]) (g.[i]) (b.[i]) (e.[i]))

    let data = [ for i in 1..h -> read_row() ]
    printfn "data: %A" (data.[data.Length - 3 .. data.Length - 1])
    Array.concat data
