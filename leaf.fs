module ProcPlant.Leaf
open DawnLib.Base
open OpenTK
open OpenTK.Graphics

type Leaf =
    | Simple
    | Birch
    | Oak
    | Bud
with
    member this.Triangles () = this.Triangles (Matrix4.Identity)
    member this.Triangles (m:Matrix4) =
        match this with
        | Bud -> let box = Cube(col=Color4.Red) in box.Triangles
        | _ ->
            [ let c, s = Color4.Green, 1.0f
              let apply = applyMat m
              yield new Vertex(col=c, pos=apply(V3(-0.2f, -1.0f, 0.0f)))
              yield new Vertex(col=c, pos=apply(V3(0.0f, 1.0f, 0.0f)))
              yield new Vertex(col=c, pos=apply(V3(1.0f, 0.0f, 0.0f))) ]
