#version 330

layout (location=0) in vec3 pos;
layout (location=1) in vec4 col;
layout (location=2) in vec3 nor;
layout (location=3) in vec2 uv;
layout (location=4) in vec3 bary;

out vec4 _col;
out vec3 _pos;
out vec3 _bary;
out vec3 _nor;

uniform mat4 mv;
uniform mat4 p;
uniform vec3 camera;

void main() {
    vec4 pp = vec4(pos, 1);
    vec4 pp0 = mv * pp;
    _pos = pp0.xyz;
    _bary = bary;
    _nor = nor;
    _col = col;
    gl_Position = vec4(_pos, 1);
}
