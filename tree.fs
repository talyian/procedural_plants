module ProcPlant.Tree
#nowarn "58"
open DawnLib.Base
open OpenTK
open OpenTK.Graphics

type Branch(p1, p2, maxwidth, order) =
    member this.P1 = p1
    member this.P2 = p2
    member this.Order = order
    member val Width = 0.0f with get, set
    member val Occlusion = 0.0f with get, set
    member val Parent: Branch option = None with get, set
    member this.Grow f = if this.Width < maxwidth then this.Width <- f (this.Width)
    member this.Triangles =
        let col = Color4.BlanchedAlmond.Mult(0.5f ** this.Occlusion);
        let ring p r (norm:Vector3) = [
            let x = Vector3.Cross(norm, Vector3.UnitZ).Normalized()
            let y = Vector3.Cross(norm, x).Normalized()
            for i in 0 .. 8 do
                let angle = 6.2832f * float32 (i % 8) / float32 8
                yield p + r * sin angle * x + r * cos angle * y ]
        let c:Vector3 = V3.Normalize(V3.Cross(p2 - p1, Vector3.UnitZ)) * this.Width * 0.5f
        let d = V3.Normalize(p2 - p1)
        match this.Parent with
            | None ->
                bridged (ring p1 this.Width d) (ring p2 this.Width d) |> List.map(fun p -> Vertex(pos=p,col=col))
            | Some(parent:Branch) ->
                let a = bridged (ring p1 this.Width d) (ring p2 this.Width d)
                let b = bridged (ring parent.P2 parent.Width (parent.P2 - parent.P1)) (ring p1 this.Width d)
                a @ b |> List.map (fun p -> Vertex(pos=p, col=col))

type Bud (p,d,pw, o, lb) =
    member val pos = p
    member val dir = d
    member val power = pw
    member val order = o
    member val LastBranch = lb with get, set
    new () = Bud(V3.Zero, V3.UnitY, 1.0f, 0, None)

type Leaf(pos, dir:Vector3) =
    member this.Pos = pos
    member this.Dir = dir
    member val Occlusion = 0.0f with get, set
    member this.Triangles =
        let col = Color4(0.1f,0.6f,0.2f,0.3f).Mult(0.5f ** this.Occlusion)
        let w = 0.02f
        let o = 0.08f
        let rand = new System.Random(this.GetHashCode())
        [ for i in 1 .. rand.Next(3, 3) do
            let dir = dir.Normalized () + rand.NextV3() * 0.3f
            let dir2 = Vector3.Cross(dir, Vector3.UnitY).Normalized()
            let offset = rand.NextV3() * o - 0.5f * o * Vector3.One

            yield Vertex(pos=pos + offset, col=col)
            yield Vertex(pos=pos + offset + dir * w, col=col)
            yield Vertex(pos=pos + offset + dir2 * w, col=col)
            yield Vertex(pos=pos + offset + dir2 * w, col=col)
            yield Vertex(pos=pos + offset + dir * w, col=col)
            yield Vertex(pos=pos + offset + w * (dir + dir2 + Vector3.UnitY), col=col)
        ]

type Tree =
    | TreeBud of Bud
    | Branch of Branch
    | TreeLeaf of Leaf
with
    member this.Triangle = match this with
    | TreeBud(bud) -> [ ]
    | TreeLeaf (leaf) -> leaf.Triangles
    | Branch(b) -> b.Triangles

module Trees =

    let pts_to_branches pts =
        let branches = List.pairwise pts |> List.map(fun (a,b) -> new Branch(a, b, 0.03f, 0))
        branches |> List.pairwise |> List.iter (fun (b1, b2) -> b2.Parent <- Some(b1))
        branches

    let start_power = 0.8f  // this determines the maximum length of a branch from the root
    let seg_dist = 0.02f    // every tick, a bud moves this far
    let branch_factor = 10.f * seg_dist // rate of branching
    let branch_angle = 0.5f  // maximum angle of a fork
    let branch_width = 0.0f // initial thickness of trunk
    let branch_growth n = n + 0.05f * seg_dist // growth function of trunk
    let gravitropism = 0.1f * seg_dist // tendency to grow up
    let phototopism = 3 // tendency to grow towards free space
    let branch_power n rand =
        let f = 0.1f in n * (1.f - nextf rand f) // chance for branch to be shorter

    let do_step rand max_age (space:SpatialIndex) state =
        let step  = function
            | TreeBud(bud) ->
                [
                let pos, dir, pow = bud.pos, bud.dir, bud.power
                let newpos = pos + dir * seg_dist
                let newpow = pow - seg_dist
                let width = branch_width - 0.05f
                let newbranch = new Branch(pos, newpos, bud.power, bud.order)
                newbranch.Parent <- bud.LastBranch
                yield Branch(newbranch)
                bud.LastBranch <- Some(newbranch)
                space.Add (Box(pos, newpos, Unchecked.defaultof<Mesh>))
                if newpow > 0.0f then
                    let newdir () =
                        let q = Quaternion.FromAxisAngle(nextv rand, nextf rand branch_angle)
                        let v = V3.Transform(dir, q)
                        Vector3.Lerp(v, Vector3.UnitY, gravitropism)
                    yield TreeBud(Bud(newpos, (newdir()), newpow, bud.order, bud.LastBranch))
                    if nextf rand 1.0f < branch_factor then
                       yield TreeBud(Bud(newpos, (newdir()), branch_power newpow rand, bud.order + 1, bud.LastBranch))
                if newpow < 0.3f then
                    yield TreeLeaf(Leaf(newpos, dir))
                ]
            | Branch(b) -> b.Grow branch_growth; [ Branch(b) ]
            | _ as br -> [ br ]
        List.collect step state

    let buds_remain st = List.exists(function | TreeBud(_) -> true | _ -> false) st
    let start_state = [TreeBud(Bud(Vector3.Zero, Vector3.UnitY, start_power, 0, None))]

    let do_make seed max_age =
        let rand = new System.Random(defaultArg seed (__rand.Next()))
        let space:SpatialIndex = upcast new ListIndex()
        let tree =
            start_state
            |> Seq.unfold (do_step rand max_age space >> fun next ->Some(next, next))
            |> Seq.find (not << buds_remain)
        for item in tree do match item with
            | Branch(br) ->
                br.Occlusion <-
                    space.Find None |> Seq.sumBy(fun box ->
                        if (box.Max - br.P2).Length > 0.2f then 0.0f
                        else 0.001f
                    )
            | TreeLeaf(t) ->
                t.Occlusion <-
                    space.Find None |> Seq.sumBy(fun box ->
                        if (box.Max - t.Pos).Length > 0.2f then 0.0f
                        else 0.001f)
            | _ -> ()
        let mesh = tree |> List.collect(fun tt -> tt.Triangle)|> Mesh.fromTriangles
        mesh.CalcNormals()
        mesh
